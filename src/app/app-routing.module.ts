import {NgModule} from '@angular/core';
import {Data, RouterModule, Routes} from '@angular/router';
import {UsersComponent} from './users/users.component';
import {ServersComponent} from './servers/servers.component';
import {EditServerComponent} from './servers/edit-server/edit-server.component';
import {ServerComponent} from './servers/server/server.component';
import {HomeComponent} from './home/home.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {AppComponent} from './app.component';
import {AuthGuard} from './auth-guard.service';
import {CanDeactivateGuard} from './servers/edit-server/can-deactivate-guard.service';
import {ErrorPageComponent} from './error-page/error-page.component';
import {ServerResolver} from './servers/server/server-resolver.service';

const appRoutes: Routes = [
  {path: 'users/:idUser/:nameUser', component: UsersComponent},
  {path: 'users', component: UsersComponent},
  {path: 'servers',
    // canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: ServersComponent,
    children: [
      // child routes of servers
      {path: ':id/edit', component: EditServerComponent, canDeactivate: [CanDeactivateGuard]},
      {path: ':id', component: ServerComponent, resolve: {server: ServerResolver}}
    ] },
  {path: 'home', component: HomeComponent},
  // {path: 'something', component: PageNotFoundComponent},
  // data =>to pass static data to component
  {path: 'something', component: ErrorPageComponent, data: {message: 'Page not Found!!!!'}},
  // to catch all inconvinient paths we dont know. and this should be last as it searches the
  // clicked link from start of array
  {path: '**', redirectTo: '/something'}
];


@NgModule({
  imports: [
    // {useHash: true} => javascript object that tell the web server to use oly the part
    // of url that is before the hash sign
    // http://localhost:4200/#/home => that is if home page dont load it will go to index.html
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
