// this is to see whether we are allowed to leave or not the page when something changes
// using canDeactivate()
import {Observable} from 'rxjs';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot} from '@angular/router';

export interface CanComponentDeactivate {
  canDeactivate_1: () => Observable<boolean> | boolean | Promise<boolean> ;
}
// CanDeactivate is a generic interface which accepts our own created interface
export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {
  // canDeactivate =>call by angular router when we leave the route
  // tslint:disable-next-line:max-line-length
  canDeactivate(component: CanComponentDeactivate, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return component.canDeactivate_1();
  }

}
