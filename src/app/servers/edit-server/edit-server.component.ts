import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {CanComponentDeactivate} from './can-deactivate-guard.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-edit-server',
  templateUrl: './edit-server.component.html',
  styleUrls: ['./edit-server.component.css']
})
export class EditServerComponent implements OnInit, CanComponentDeactivate {
   allowEdit = false;
   changessaved = false;
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    // retrieving query parameters and fragments
    console.log(this.route.snapshot.queryParams);
    console.log(this.route.snapshot.fragment);
    this.route.queryParams.subscribe(
      (queryParams: Params) => {
        // [queryParams]="{allow:.........}"
        this.allowEdit = queryParams.allow === '1' ? true : false;
      }
    );
    this.route.fragment.subscribe();
  }


  update() {
    this.changessaved = true;
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  canDeactivate_1(): Observable<boolean> | boolean | Promise<boolean> {
    if (!this.allowEdit) {
      return  true;
    }
    if (this.changessaved === false) {
      return confirm('Do you want to leave the page');
    } else {
      return true;
    }
  }
}
