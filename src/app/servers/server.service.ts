
export class ServerService {
  mess: string;
  server: [
    {id: 1, name: 'a', status: 'on'},
    {id: 2, name: 'b', status: 'on'},
    {id: 3, name: 'c', status: 'off'},
  ]

  getServer() {
    this.mess = 'successful';
    return {id: 4, name: 'd', status: 'on'};
  }
}
