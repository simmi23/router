import { Component, OnInit } from '@angular/core';
import {ServerService} from '../server.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {EditServerComponent} from '../edit-server/edit-server.component';



@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
   server: {id: number, name: string, status: string}
  constructor(private serverService: ServerService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
     const id = this.route.snapshot.params.id;
     // @ts-ignore
     this.server = this.serverService.getServer(1);
     this.route.params.subscribe(
       (params: Params) => {
         // @ts-ignore
         this.server = this.serverService.getServer(params.id);
       }
     );
  }

  onEdit() {
     // relatively loading as edit is child
    this.router.navigate(['edit'], {relativeTo: this.route, queryParamsHandling: 'preserve'});
  }
}
