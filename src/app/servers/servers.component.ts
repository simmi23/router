import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {relativeToRootDirs} from '@angular/compiler-cli/src/transformers/util';
import {ServerService} from './server.service';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  servers = [
    {
      id: 1,
      name: 'Sanika'
    },
    {
      id : 2,
      name: 'Aparna'
    },
    {
      id: 3,
      name: 'Simran'
    }
  ];
  // ActivatedRoute injects the currently active route, so for the component we loaded
  // this will be the route  which loaded this component
  constructor(private router: Router, private route: ActivatedRoute) { }
  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.servers = data.server;
      }
    );
  }

  onReload() {
    // Bcoz RouterLink knows on which component we are currently on and navigate dont know
    // this.router.navigate(['servers']); not giving error like RouterLink as / is not added
    // so use second argument as javascript object in navigate()
    // ------------------------------------------------------------
    // this.router.navigate(['servers'], {relativeTo: this.route}); // will give error as / is not added
  }
}
