import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
   user: {id: number, name: string}
   paramSubscription: Subscription;
  constructor(private route: ActivatedRoute) { }

   ngOnInit() {
     this.user = {
       // params is javascript object
       // idUser, nameUser ->defined in app.module.ts as a url parameter
       id: this.route.snapshot.params.idUser,
       name: this.route.snapshot.params.nameUser
     };
     // params are observables which are not added by angular but are third party pacakage,but
    // use by angular to deal with asynchronous tasks(parameters might change but dont know when, where and how long it
    // will take to  change or may be not change so we cannot block code)
    // so observable is an easy way to subscribe events which might
    // happen in future
     this.paramSubscription = this.route.params.subscribe(
       // 1st parameter is the value that is changing
       (params: Params) => {
         this.user.id = params.idUser;
         this.user.name = params.nameUser;
       }
     );
     // if we know that our component is not loaded again from inside the
    // component again then dont need to subscribe
   }
   // When we leave this component and moving to other component then component is destroying
   // but the subscription is not destroying so we have to manually destroy it by ngOnDestroy
  // when this component is destroyed.
   // destroy the subscription
   ngOnDestroy(): void {
    this.paramSubscription.unsubscribe();
   }

}
